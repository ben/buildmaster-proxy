package main

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"sync"
)

var hookSecret = []byte(os.Getenv("GITHUB_HOOK_SECRET"))

// Only one hook gets handled at a time.
var hookLock sync.Mutex

func GitHubHook(w http.ResponseWriter, r *http.Request) {
	if !strings.HasPrefix(r.UserAgent(), "GitHub-Hookshot/") {
		http.Error(w, "This is the GitHub hook endpoint.", http.StatusForbidden)
		return
	}

	log.Println("[webhook]", r.Method, r.URL.Path)

	signature, err := hex.DecodeString(strings.TrimPrefix(r.Header.Get("X-Hub-Signature"), "sha1="))
	if err != nil {
		log.Println("[webhook] malformed signature")
		http.Error(w, "signature: "+err.Error(), http.StatusInternalServerError)
		return
	}

	payloadBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println("[webhook] failed to read payload")
		http.Error(w, "body: "+err.Error(), http.StatusInternalServerError)
		return
	}

	hash := hmac.New(sha1.New, hookSecret)
	_, _ = hash.Write(payloadBytes)
	if !hmac.Equal(signature, hash.Sum(nil)) {
		log.Println("[webhook] incorrect signature")
		http.Error(w, "Invalid signature.", http.StatusBadRequest)
		return
	}

	hookLock.Lock()
	defer hookLock.Unlock()

	switch r.Header.Get("X-GitHub-Event") {
	case "push":
		var payload struct {
			Ref        string `json:"ref"`
			After      string `json:"after"`
			Repository struct {
				FullName string `json:"full_name"`
			} `json:"repository"`
		}

		if err = json.Unmarshal(payloadBytes, &payload); err != nil {
			log.Println("[webhook] malformed push event")
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		switch payload.Repository.FullName {
		case "DFHack/dfhack":
			CoreCommit(payload.Ref, payload.After)
		case "BenLubar/df-ai":
			PluginCommit("df-ai", payload.Ref, payload.After)
		case "BenLubar/weblegends":
			PluginCommit("weblegends", payload.Ref, payload.After)
		case "BenLubar/bingo":
			PluginCommit("bingo", payload.Ref, payload.After)
		case "white-rabbit-dfplex/dfplex":
			PluginCommit("dfplex", payload.Ref, payload.After)
		case "thurin/df-twbt":
			PluginCommit("twbt", payload.Ref, payload.After)
		default:
			log.Println("[webhook] bad commit repo", payload.Repository.FullName)
		}
	case "create":
		var payload struct {
			RefType    string `json:"ref_type"`
			Ref        string `json:"ref"`
			Repository struct {
				FullName string `json:"full_name"`
			} `json:"repository"`
		}

		if err = json.Unmarshal(payloadBytes, &payload); err != nil {
			log.Println("[webhook] malformed create event")
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		if payload.RefType == "tag" {
			switch repo := payload.Repository.FullName; repo {
			case "DFHack/dfhack":
				CoreTag(payload.Ref)
			case "BenLubar/df-ai":
				PluginTag(repo, "df-ai", payload.Ref)
			case "BenLubar/weblegends":
				PluginTag(repo, "weblegends", payload.Ref)
			case "BenLubar/bingo":
				PluginTag(repo, "bingo", payload.Ref)
			case "white-rabbit-dfplex/dfplex":
				PluginTag(repo, "dfplex", payload.Ref)
			case "thurin/df-twbt":
				PluginTag(repo, "twbt", payload.Ref)
			default:
				log.Println("[webhook] bad tag repo", repo)
			}
		}
	case "pull_request":
		var payload struct {
			Action      string      `json:"action"`
			Number      int         `json:"number"`
			PullRequest PullRequest `json:"pull_request"`
		}
		if err = json.Unmarshal(payloadBytes, &payload); err != nil {
			log.Println("[webhook] malformed pull_request event")
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		var fn func(string, string, string, string)
		switch payload.Action {
		case "opened":
			fn = payload.PullRequest.OnOpen
		case "closed":
			fn = payload.PullRequest.OnClose
		case "reopened":
			fn = payload.PullRequest.OnReopen
		case "synchronize":
			fn = payload.PullRequest.OnSynchronize
		default:
			log.Println("[webhook] ignoring PR", payload.Number, "action", payload.Action)
			w.WriteHeader(http.StatusNoContent)
			return
		}

		var applicationName string
		var pipelineName string
		commitVariable := "$DFHackCommit"
		expectedBranch := "master"

		switch payload.PullRequest.Base.Repo.FullName {
		case "DFHack/dfhack":
			applicationName = "DFHack Pull Requests"
			expectedBranch = "develop"
			pipelineName = "dfhack-raft::DFHack-Core-Pull-Request"
		case "DFHack/df-structures":
			applicationName = "df-structures Pull Requests"
			commitVariable = "$OverrideStructuresCommit"
			pipelineName = "dfhack-raft::DFHack-Core-Pull-Request"
		case "BenLubar/df-ai":
			applicationName = "df-ai Pull Requests"
			commitVariable = "$PluginCommit"
			pipelineName = "dfhack-raft::DFHack-Plugin-Pull-Request"
			expectedBranch = "develop"
		case "BenLubar/weblegends":
			applicationName = "weblegends Pull Requests"
			commitVariable = "$PluginCommit"
			pipelineName = "dfhack-raft::DFHack-Plugin-Pull-Request"
			expectedBranch = "develop"
		case "BenLubar/bingo":
			applicationName = "bingo Pull Requests"
			commitVariable = "$PluginCommit"
			pipelineName = "dfhack-raft::DFHack-Plugin-Pull-Request"
			expectedBranch = "develop"
		case "white-rabbit-dfplex/dfplex":
			applicationName = "dfplex Pull Requests"
			commitVariable = "$PluginCommit"
			pipelineName = "dfhack-raft::DFHack-Plugin-Pull-Request"
		case "thurin/df-twbt":
			applicationName = "twbt Pull Requests"
			commitVariable = "$PluginCommit"
			pipelineName = "dfhack-raft::DFHack-Plugin-Pull-Request"
		default:
			log.Println("[webhook] bad PR", payload.Number, "repo", payload.PullRequest.Base.Repo.FullName)
			w.WriteHeader(http.StatusNoContent)
			return
		}

		if payload.PullRequest.Base.Ref != expectedBranch && payload.Action != "closed" {
			log.Println("[webhook] unexpected PR", payload.Number, "branch", payload.PullRequest.Base.Ref)
			w.WriteHeader(http.StatusNoContent)
			return
		}

		fn(pipelineName, applicationName, commitVariable, strconv.Itoa(payload.Number))
	}

	w.WriteHeader(http.StatusNoContent)
}

func CoreCommit(ref, sha string) {
	if ref != "refs/heads/develop" {
		log.Println("[webhook] ignoring commit to dfhack core", ref)
		return
	}

	build, err := CreateDevelopmentBuild("DFHack", "dfhack-raft::DFHack", url.Values{
		"$DFHackCommit": {sha},
	})
	if err != nil {
		panic(err)
	}

	log.Println("[webhook] queued build", build.ID, "for dfhack core commit", sha)
}

func GetLatestCommitOnBranch(branch, repo string) (string, error) {
	r, err := http.Get("https://api.github.com/repos/" + repo + "/branches/" + branch)
	if err != nil {
		return "", err
	}
	defer r.Body.Close()

	var data struct {
		Commit struct {
			Sha string `json:"sha"`
		} `json:"commit"`
	}
	err = json.NewDecoder(r.Body).Decode(&data)
	return data.Commit.Sha, err
}

func PluginCommit(plugin, ref, sha string) {
	if ref != "refs/heads/master" && ref != "refs/heads/develop" {
		log.Println("[webhook] ignoring commit to plugin", plugin, ref)
		return
	}

	coreSha, err := GetLatestCommitOnBranch("develop", "DFHack/dfhack")
	if err != nil {
		panic(err)
	}

	build, err := CreateDevelopmentBuild(plugin, "dfhack-raft::DFHack-Plugin", url.Values{
		"$DFHackCommit": {coreSha},
		"$PluginCommit": {sha},
	})
	if err != nil {
		panic(err)
	}

	log.Println("[webhook] queued build", build.ID, "for plugin", plugin, "commit", sha)

	coreTag, coreSha, err := GetTag("", "DFHack/dfhack")
	if err != nil {
		panic(err)
	}

	if strings.HasSuffix(coreTag, "-alpha0") {
		return
	}

	release, err := EnsureRelease(plugin, "dfhack-"+coreTag, url.Values{
		"pipelineName":  {"dfhack-raft::DFHack-Plugin"},
		"releaseName":   {"DFHack " + coreTag},
		"$DFHackBranch": {"develop"},
		"$DFHackTag":    {coreTag},
		"$PluginTagged": {"false"},
	})
	if err != nil {
		panic(err)
	}

	if release.Status != "active" {
		return
	}

	build, err = CreateReleaseBuild(release.ID, url.Values{
		"$DFHackCommit": {coreSha},
		"$PluginCommit": {sha},
	})
	if err != nil {
		panic(err)
	}

	log.Println("[webhook] queued build", build.ID, "for plugin", plugin, "commit", sha, "(dfhack", coreTag, "tag)")
}

func DeployReleases(application string) {
	var releases []BMRelease
	if err := APIRequest("/api/releases", url.Values{
		"applicationName": {application},
		"status":          {"active"},
	}, &releases); err != nil {
		panic(err)
	}

	for _, release := range releases {
		if release.Number == "0.0.0" {
			// Don't deploy legacy develop release
			continue
		}
		if strings.HasPrefix(release.Number, "dfhack-") {
			// Don't deploy prereleases
			continue
		}

		var ignore interface{}
		if err := APIRequest("/api/releases/packages/deploy", url.Values{
			"packageId": {strconv.Itoa(release.LatestPackageID)},
			"toStage":   {"Deployed"},
			"force":     {"true"},
		}, &ignore); err != nil {
			panic(err)
		}
	}
}

func GetTag(expectedName, repo string) (name, sha string, err error) {
	r, err := http.Get("https://api.github.com/repos/" + repo + "/tags")
	if err != nil {
		return "", "", err
	}
	defer r.Body.Close()

	var tags []struct {
		Name   string `json:"name"`
		Commit struct {
			Sha string `json:"sha"`
		} `json:"commit"`
	}

	err = json.NewDecoder(r.Body).Decode(&tags)
	if err != nil {
		return "", "", err
	}

	for _, tag := range tags {
		if tag.Name == expectedName {
			return tag.Name, tag.Commit.Sha, nil
		}
	}

	return tags[0].Name, tags[0].Commit.Sha, nil
}

func CoreTag(tag string) {
	if strings.HasSuffix(tag, "-alpha0") {
		log.Println("[webhook] ignoring core tag - ends in alpha0:", tag)
		return
	}

	DeployReleases("DFHack")

	_, coreSha, err := GetTag(tag, "DFHack/dfhack")
	if err != nil {
		panic(err)
	}

	release, err := EnsureRelease("DFHack", tag, url.Values{
		"pipelineName":  {"dfhack-raft::DFHack"},
		"$DFHackBranch": {"develop"},
		"$DFHackTag":    {tag},
		"$DFHackTagged": {"true"},
	})
	if err != nil {
		panic(err)
	}

	build, err := CreateReleaseBuild(release.ID, url.Values{
		"$DFHackCommit": {coreSha},
	})
	if err != nil {
		panic(err)
	}

	log.Println("[webhook] queued build", build.ID, "for dfhack core tag", tag)
}

func PluginTag(repo, plugin, tag string) {
	DeployReleases(plugin)

	coreTag, coreSha, err := GetTag("", "DFHack/dfhack")
	if err != nil {
		panic(err)
	}
	_, pluginSha, err := GetTag(tag, repo)
	if err != nil {
		panic(err)
	}

	release, err := EnsureRelease(plugin, tag, url.Values{
		"pipelineName":  {"dfhack-raft::DFHack-Plugin"},
		"$DFHackBranch": {"develop"},
		"$DFHackTag":    {coreTag},
		"$PluginTag":    {tag},
		"$PluginTagged": {"true"},
	})
	if err != nil {
		panic(err)
	}

	build, err := CreateReleaseBuild(release.ID, url.Values{
		"$DFHackCommit": {coreSha},
		"$PluginCommit": {pluginSha},
	})
	if err != nil {
		panic(err)
	}

	log.Println("[webhook] queued build", build.ID, "for plugin", plugin, "tag", tag)
}

type PullRequest struct {
	Title string `json:"title"`
	Body  string `json:"body"`
	Head  struct {
		Sha  string `json:"sha"`
		Repo struct {
			Name  string `json:"name"`
			Owner struct {
				Login string `json:"login"`
			} `json:"owner"`
		} `json:"repo"`
	} `json:"head"`
	Base struct {
		Ref  string `json:"ref"`
		Repo struct {
			FullName string `json:"full_name"`
		} `json:"repo"`
	} `json:"base"`
	Merged bool `json:"merged"`
}

func (pr *PullRequest) createBuild(pipelineName, applicationName, commitVariable, number string) (*BMBuild, error) {
	release, err := EnsureRelease(applicationName, number, url.Values{
		"pipelineName": {pipelineName},
		"$ForkOwner":   {pr.Head.Repo.Owner.Login},
		"$ForkRepo":    {pr.Head.Repo.Name},
		"$PullRequest": {pr.Title},
	})
	if err != nil {
		return nil, err
	}

	return CreateReleaseBuild(release.ID, url.Values{
		commitVariable: {pr.Head.Sha},
	})
}

func (pr *PullRequest) OnOpen(pipelineName, applicationName, commitVariable, number string) {
	if strings.Contains(pr.Body, "buildmaster:skip-pr") {
		log.Println("[webhook] skipping", applicationName, "PR", number, "as requested")
		return
	}

	build, err := pr.createBuild(pipelineName, applicationName, commitVariable, number)
	if err != nil {
		panic(err)
	}

	log.Println("[webhook] created build", build.ID, "for newly opened", applicationName, "PR", number)
}

func (pr *PullRequest) OnClose(pipelineName, applicationName, commitVariable, number string) {
	if pr.Merged {
		var releases []BMRelease
		if err := APIRequest("/api/releases", url.Values{
			"applicationName": {applicationName},
			"releaseNumber":   {number},
		}, &releases); err != nil {
			panic(err)
		}

		for _, release := range releases {
			var ignore interface{}
			if err := APIRequest("/api/releases/packages/deploy", url.Values{
				"buildId": {fmt.Sprint(release.LatestPackageID)},
				"toStage": {"Merged"},
				"force":   {"true"},
			}, &ignore); err != nil {
				panic(err)
			}
		}

		log.Println("[webhook]", applicationName, "PR", number, "was merged")
	} else {
		var ignore interface{}
		if err := APIRequest("/api/releases/cancel", url.Values{
			"applicationName": {applicationName},
			"releaseNumber":   {number},
		}, &ignore); err != nil {
			log.Println(err)
		}

		log.Println("[webhook]", applicationName, "PR", number, "was closed")
	}
}

func (pr *PullRequest) OnReopen(pipelineName, applicationName, commitVariable, number string) {
	var ignore interface{}
	if err := APIRequest("/api/releases/restore", url.Values{
		"applicationName": {applicationName},
		"releaseNumber":   {number},
	}, &ignore); err != nil {
		log.Println(err)
		return
	}

	build, err := pr.createBuild(pipelineName, applicationName, commitVariable, number)
	if err != nil {
		panic(err)
	}

	log.Println("[webhook] created build", build.ID, "for reopened", applicationName, "PR", number)
}

func (pr *PullRequest) OnSynchronize(pipelineName, applicationName, commitVariable, number string) {
	build, err := pr.createBuild(pipelineName, applicationName, commitVariable, number)
	if err != nil {
		panic(err)
	}

	log.Println("[webhook] created build", build.ID, "for updated", applicationName, "PR", number)
}
