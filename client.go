package main

import (
	"context"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
)

type Client struct {
	*http.Client

	// BaseURL is the URL of the BuildMaster instance with no path.
	// (eg. https://buildmaster.example.com)
	BaseURL string
}

var DefaultClient = &Client{
	Client: http.DefaultClient,
}

var activeExecutionRegex = regexp.MustCompile(`"/executions/execution-in-progress\?executionId=([0-9]+)"[^>]*>([^<]+)</a>`)

func (c *Client) ScrapeActiveExecutions(ctx context.Context) ([]ActiveExecution, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, c.BaseURL, nil)
	if err != nil {
		return nil, err
	}
	resp, err := c.Do(req)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != http.StatusOK {
		return nil, c.handleError(resp)
	}

	b, err1 := ioutil.ReadAll(resp.Body)
	err2 := resp.Body.Close()

	if err1 != nil {
		return nil, err1
	}
	if err2 != nil {
		return nil, err2
	}

	var executions []ActiveExecution
	for _, m := range activeExecutionRegex.FindAllSubmatch(b, -1) {
		id, err := strconv.Atoi(string(m[1]))
		if err != nil {
			return nil, err
		}
		executions = append(executions, ActiveExecution{
			ExecutionID: id,
			Title:       string(m[2]),
		})
	}

	return executions, nil
}

func (c *Client) GetProgress(ctx context.Context, executionID, nextLogSequence int) (*GetProgressResponse, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, c.BaseURL+"/0x44/BuildMaster.Web.WebApplication/Inedo.BuildMaster.Web.WebApplication.Pages.Executions.ExecutionInProgressPage/GetProgress", strings.NewReader(url.Values{
		"executionId":     {strconv.Itoa(executionID)},
		"nextLogSequence": {strconv.Itoa(nextLogSequence)},
		"showDebug":       {"true"},
	}.Encode()))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded; charset=utf-8")
	resp, err := c.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, c.handleError(resp)
	}

	var v GetProgressResponse
	err1 := json.NewDecoder(resp.Body).Decode(&v)
	err2 := resp.Body.Close()
	if err1 != nil {
		return nil, err1
	}
	if err2 != nil {
		return nil, err2
	}
	return &v, nil
}

func (c *Client) handleError(resp *http.Response) error {
	// TODO
	_ = resp.Body.Close()
	return errors.New("request returned " + resp.Status)
}
