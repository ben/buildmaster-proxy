package main

import (
	"bytes"
	"compress/flate"
	"context"
	"encoding/base64"
	"io"
	"net/http"
	"net/http/httputil"
	"strings"
	"time"
)

func main() {
	proxyBuildMaster := &httputil.ReverseProxy{
		Director:      BuildMaster.Director,
		FlushInterval: 100 * time.Millisecond,
	}

	DefaultClient.BaseURL = "http://" + BuildMaster.Host

	var mux http.ServeMux
	mux.HandleFunc("/error", ErrorHandler)
	mux.HandleFunc("/hooks/github", GitHubHook)
	mux.Handle("/lubar/events", DefaultWatcher)
	mux.Handle("/", proxyBuildMaster)

	go DefaultWatcher.Watch(context.Background())

	panic(http.ListenAndServe(":8081", &mux))
}

var BuildMaster = &Proxy{
	Host:     "192.168.1.100:8622",
	LogInAPI: "BuildMaster.Web.WebApplication/Inedo.BuildMaster.Web.WebApplication.Pages.LogInPage/LogIn",
}

type Proxy struct {
	Host     string
	LogInAPI string
}

func (p *Proxy) Director(req *http.Request) {
	req.URL.Scheme = "http"
	req.URL.Host = p.Host
	req.Header.Set("User-Agent", req.Header.Get("User-Agent")+" (forwarded-for/"+req.Header.Get("X-Forwarded-For")+") (cloudflare-geo/"+req.Header.Get("CF-IPCountry")+")")
	//log.Println("[proxy]", req.Method, req.URL.Path)
}

func ErrorHandler(w http.ResponseWriter, r *http.Request) {
	b64Message := r.URL.Query().Get("fullMessage")
	if b64Message == "" {
		http.Error(w, "missing parameter: fullMessage", http.StatusBadRequest)
		return
	}
	sr := strings.NewReader(b64Message)
	dec := base64.NewDecoder(base64.StdEncoding, sr)
	inf := flate.NewReader(dec)

	var buf bytes.Buffer
	buf.WriteString("buildmaster.lubar.me: Intercepted error message:\n\n")
	if _, err := io.CopyN(&buf, inf, 1<<20); err == nil {
		buf.WriteString("\n\nWARNING: message truncated due to length")
	} else if err != io.EOF {
		http.Error(w, "message failed to decode: "+err.Error(), http.StatusBadRequest)
		return
	}
	http.Error(w, buf.String(), http.StatusInternalServerError)
}
