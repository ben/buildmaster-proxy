package main

import "time"

type Timer struct {
	Current time.Duration

	// limits (only applied when moving in the correct direction)
	MinSuccess  time.Duration
	MaxNoChange time.Duration
	MaxFailure  time.Duration

	// number of seconds (new) per second (old)
	ScaleSuccess  time.Duration
	ScaleNoChange time.Duration
	ScaleFailure  time.Duration
}

func (t *Timer) Success() {
	if t.Current <= t.MinSuccess {
		t.Current = t.MinSuccess
		return
	}

	t.Current = t.Current * t.ScaleSuccess / time.Second
	if t.Current < t.MinSuccess {
		t.Current = t.MinSuccess
	}
}

func (t *Timer) NoChange() {
	if t.Current == t.MaxNoChange {
		return
	}

	if t.Current > t.MaxNoChange {
		t.Success()
		if t.Current <= t.MaxNoChange {
			t.Current = t.MaxNoChange
		}
		return
	}

	t.Current = t.Current * t.ScaleNoChange / time.Second
	if t.Current > t.MaxNoChange {
		t.Current = t.MaxNoChange
	}
}

func (t *Timer) Failure() {
	if t.Current >= t.MaxFailure {
		t.Current = t.MaxFailure
		return
	}

	t.Current = t.Current * t.ScaleFailure / time.Second
	if t.Current > t.MaxFailure {
		t.Current = t.MaxFailure
	}
}
