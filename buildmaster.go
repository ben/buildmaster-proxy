package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"

	"github.com/pkg/errors"
)

func APIRequest(path string, payload url.Values, result interface{}) error {
	payload.Set("key", os.Getenv("BUILDMASTER_API_KEY"))

	r, err := http.Post("http://"+BuildMaster.Host+path, "application/x-www-form-urlencoded", strings.NewReader(payload.Encode()))
	if err != nil {
		return errors.Wrap(err, "performing POST request")
	}
	defer r.Body.Close()

	if r.StatusCode >= 400 {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			return errors.Wrapf(err, "reading HTTP %d response body", r.StatusCode)
		}
		return errors.Errorf("HTTP %d error: %s", r.StatusCode, string(body))
	}

	err = json.NewDecoder(r.Body).Decode(result)
	return errors.Wrap(err, "decoding JSON body")
}

type BMRelease struct {
	ID              int    `json:"id"`
	Number          string `json:"number"`
	Status          string `json:"status"`
	LatestPackageID int    `json:"latestPackageId"`
}

type BMBuild struct {
	ID int `json:"id"`
}

func EnsureRelease(applicationName, releaseNumber string, payload url.Values) (*BMRelease, error) {
	var releases []BMRelease
	err := APIRequest("/api/releases", url.Values{
		"applicationName": {applicationName},
		"releaseNumber":   {releaseNumber},
	}, &releases)
	if err != nil {
		return nil, err
	}

	if len(releases) != 0 {
		return &releases[0], nil
	}

	payload.Set("applicationName", applicationName)
	payload.Set("releaseNumber", releaseNumber)

	var release BMRelease
	if err = APIRequest("/api/releases/create", payload, &release); err != nil {
		return nil, err
	}
	return &release, nil
}

func CreateReleaseBuild(releaseID int, payload url.Values) (*BMBuild, error) {
	payload.Set("releaseId", strconv.Itoa(releaseID))

	var build BMBuild
	if err := APIRequest("/api/releases/packages/create", payload, &build); err != nil {
		return nil, err
	}

	var ignore interface{}
	if err := APIRequest("/api/releases/packages/deploy", url.Values{
		"packageId": {strconv.Itoa(build.ID)},
	}, &ignore); err != nil {
		return nil, err
	}

	return &build, nil
}

func CreateDevelopmentBuild(applicationName, pipelineName string, payload url.Values) (*BMBuild, error) {
	payload.Set("applicationName", applicationName)
	payload.Set("pipelineName", pipelineName)

	var build BMBuild
	if err := APIRequest("/api/releases/packages/create", payload, &build); err != nil {
		return nil, err
	}

	var ignore interface{}
	if err := APIRequest("/api/releases/packages/deploy", url.Values{
		"packageId": {strconv.Itoa(build.ID)},
	}, &ignore); err != nil {
		return nil, err
	}

	return &build, nil
}
