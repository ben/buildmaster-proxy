FROM golang:latest

WORKDIR /go/src/gist.github.com/BenLubar/a10d4b0890d9d5894ec4c9bb0d35258a.git
COPY . .
RUN CGO_ENABLED=0 go build -a -o buildmaster-proxy .

FROM scratch
COPY --from=0 /go/src/gist.github.com/BenLubar/a10d4b0890d9d5894ec4c9bb0d35258a.git/buildmaster-proxy /
EXPOSE 8081
CMD ["/buildmaster-proxy"]
