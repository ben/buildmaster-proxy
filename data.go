package main

import "encoding/json"

type NullInt struct {
	Value   int
	IsValid bool
}

func (ni *NullInt) MarshalJSON() ([]byte, error) {
	if ni.IsValid {
		return json.Marshal(ni.Value)
	}

	return []byte("null"), nil
}

func (ni *NullInt) UnmarshalJSON(b []byte) error {
	if string(b) == "null" {
		ni.Value = 0
		ni.IsValid = false
		return nil
	}

	err := json.Unmarshal(b, &ni.Value)
	if err == nil {
		ni.IsValid = true
	}
	return err
}

type ActiveExecution struct {
	ExecutionID int
	Title       string
}

type GetProgressResponse struct {
	Executions          []*ExecutionModel `json:"Executions"`
	RedirectURL         string            `json:"RedirectUrl"`
	ServiceError        bool              `json:"ServiceError"`
	ExecutionsEnded     bool              `json:"ExecutionsEnded"`
	AnyExecutionsFailed bool              `json:"AnyExecutionsFailed"`
}

type ExecutionModel struct {
	ID               int              `json:"Id"`
	Status           string           `json:"Status"`
	RunState         string           `json:"RunState"`
	Mode             string           `json:"Mode"`
	Title            string           `json:"Title"`
	Canceled         bool             `json:"Canceled"`
	StartTime        string           `json:"StartTime"`
	ElapsedTime      string           `json:"ElapsedTime"`
	ReexecuteURL     string           `json:"ReexecuteUrl"`
	DetailsURL       string           `json:"DetailsUrl"`
	CancelURL        string           `json:"CancelUrl"`
	PercentComplete  NullInt          `json:"PercentComplete"`
	CurrentOperation *OperationStatus `json:"CurrentOperation"`
	Log              []LogEntry       `json:"Log"`
	MaxLogSequence   NullInt          `json:"MaxLogSequence"`
}

type OperationStatus struct {
	AsyncTitle               string             `json:"AsyncTitle,omitempty"`
	ShortDescription         string             `json:"ShortDescription,omitempty"`
	LongDescription          string             `json:"LongDescription,omitempty"`
	PercentComplete          NullInt            `json:"PercentComplete"`
	BackgroundOperations     []*OperationStatus `json:"BackgroundOperations,omitempty"`
	StatementMessage         string             `json:"StatementMessage,omitempty"`
	StatementPercentComplete NullInt            `json:"StatementPercentComplete"`
	StatementStalled         bool               `json:"StatementStalled"`
}

type LogEntry struct {
	Message string `json:"Message"`
	Class   string `json:"Class"`
}
