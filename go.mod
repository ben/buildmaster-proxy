module gist.github.com/BenLubar/a10d4b0890d9d5894ec4c9bb0d35258a.git

go 1.13

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/pkg/errors v0.9.1
)
