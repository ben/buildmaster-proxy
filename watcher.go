package main

import (
	"context"
	"crypto/rand"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"
)

const cleanEventsThreshold = 5000
const keepEndMarkerFor = 12 * time.Hour

type Watcher struct {
	Client *Client

	ScrapeExecutionsDelay Timer
	GetProgressDelay      Timer

	lock        sync.Mutex
	init        sync.Once
	idPrefix    string
	eventCond   sync.Cond
	active      map[int]*WatchedExecution
	inactive    map[int]time.Time
	events      []Event
	nextEventID int64
	lastClean   int
}

type Event struct {
	ID   int64
	Type string
	EID  int
	Data string
}

type WatchedExecution struct {
	ID           int
	Title        string
	LastStatus   []SimplifiedStatus
	LastProgress *GetProgressResponse
}

type SimplifiedStatus struct {
	ID               int              `json:"I"`
	Title            string           `json:"T"`
	Mode             string           `json:"M"`
	Status           string           `json:"S"`
	RunState         string           `json:"R"`
	CurrentOperation *OperationStatus `json:"O"`
}

var DefaultWatcher = &Watcher{
	Client: DefaultClient,

	ScrapeExecutionsDelay: Timer{
		Current:       5 * time.Second,
		MinSuccess:    1 * time.Second,
		MaxNoChange:   30 * time.Second,
		MaxFailure:    15 * time.Minute,
		ScaleSuccess:  time.Second / 2,
		ScaleNoChange: time.Second * 5 / 4,
		ScaleFailure:  time.Second * 4,
	},
	GetProgressDelay: Timer{
		Current:       5 * time.Second,
		MinSuccess:    100 * time.Millisecond,
		MaxNoChange:   5 * time.Second,
		MaxFailure:    5 * time.Minute,
		ScaleSuccess:  time.Second / 5,
		ScaleNoChange: time.Second * 5 / 4,
		ScaleFailure:  time.Second * 4,
	},
}

func (w *Watcher) doInit() {
	w.eventCond.L = &w.lock
	var buf [4]byte
	_, _ = rand.Read(buf[:])
	w.idPrefix = hex.EncodeToString(buf[:]) + ":"
}

func (w *Watcher) ServeHTTP(wr http.ResponseWriter, r *http.Request) {
	w.init.Do(w.doInit)

	seenEventID := int64(-1)
	if lastEventID := r.Header.Get("Last-Event-ID"); strings.HasPrefix(lastEventID, w.idPrefix) {
		if n, err := strconv.ParseInt(lastEventID[len(w.idPrefix):], 10, 64); err == nil && n >= 0 {
			seenEventID = n
		}
	}

	wr.Header().Add("Cache-Control", "no-cache")
	wr.Header().Add("Content-Type", "text/event-stream")

	wr.Write([]byte(":)\n\n"))
	if f, ok := wr.(http.Flusher); ok {
		f.Flush()
	}

	var keepAliveLock sync.Mutex
	go func() {
		for {
			select {
			case <-r.Context().Done():
				return
			case <-time.After(time.Minute):
			}

			keepAliveLock.Lock()
			wr.Write([]byte(":\n\n"))
			if f, ok := wr.(http.Flusher); ok {
				f.Flush()
			}
			keepAliveLock.Unlock()
		}
	}()
	for {
		w.eventCond.L.Lock()
		events := w.events
		if len(events) == 0 || seenEventID >= events[len(events)-1].ID {
			w.eventCond.Wait()
			events = w.events
		}
		filter := w.getEventFilter()
		w.eventCond.L.Unlock()

		keepAliveLock.Lock()
		for _, e := range events {
			if seenEventID >= e.ID {
				continue
			}

			seenEventID = e.ID
			if filter(e) {
				data := e.Data
				if data != "" {
					data = "\ndata: " + strings.Replace(data, "\n", "\ndata: ", -1)
				}
				fmt.Fprintf(wr, "id: %s%d\ndata: %s\ndata: %d%s\n\n", w.idPrefix, e.ID, e.Type, e.EID, data)
			}
		}

		if f, ok := wr.(http.Flusher); ok {
			f.Flush()
		}
		keepAliveLock.Unlock()

		select {
		case <-r.Context().Done():
			return
		default:
		}
	}
}

func (w *Watcher) Watch(ctx context.Context) {
	w.init.Do(w.doInit)

	scrapeExecutions := time.After(0) // run on the first iteration
	getProgress := time.After(w.GetProgressDelay.Current)
	cleanUpEvents := time.NewTicker(5 * time.Minute)
	defer cleanUpEvents.Stop()

	for {
		select {
		case <-scrapeExecutions:
			w.scrapeExecutions(ctx)
			scrapeExecutions = time.After(w.ScrapeExecutionsDelay.Current)
		case <-getProgress:
			w.getProgress(ctx)
			getProgress = time.After(w.GetProgressDelay.Current)
		case <-cleanUpEvents.C:
			w.cleanUpEvents()
		case <-ctx.Done():
			return
		}
	}
}

func (w *Watcher) scrapeExecutions(ctx context.Context) {
	executionIDs, err := w.Client.ScrapeActiveExecutions(ctx)
	if err != nil {
		w.ScrapeExecutionsDelay.Failure()
		// TODO: log error
		return
	}

	w.lock.Lock()
	defer w.lock.Unlock()
	if w.active == nil {
		w.active = make(map[int]*WatchedExecution)
	}
	if w.inactive == nil {
		w.inactive = make(map[int]time.Time)
	}
	var anyChange bool
	for _, e := range executionIDs {
		id := e.ExecutionID
		if _, ok := w.active[id]; !ok {
			w.active[id] = &WatchedExecution{
				ID:    id,
				Title: e.Title,
			}
			w.dispatchEvent("execution_create", id, e.Title)
			anyChange = true
		}
	}
	for id := range w.active {
		var found bool
		for _, id2 := range executionIDs {
			if id == id2.ExecutionID {
				found = true
				break
			}
		}
		if !found {
			w.dispatchEvent("execution_destroy", id, "")
			w.inactive[id] = time.Now()
			delete(w.active, id)
			anyChange = true
		}
	}

	if anyChange {
		w.ScrapeExecutionsDelay.Success()
	} else {
		w.ScrapeExecutionsDelay.NoChange()
	}
}

func (w *Watcher) getProgress(ctx context.Context) {
	w.lock.Lock()
	defer w.lock.Unlock()

	var anyChange, anySucceed, anyFail bool

	for _, e := range w.active {
		lastLog := 0
		if e.LastProgress != nil && len(e.LastProgress.Executions) != 0 {
			lastLog = e.LastProgress.Executions[0].MaxLogSequence.Value
		}

		w.lock.Unlock()

		progress, err := w.Client.GetProgress(ctx, e.ID, lastLog)

		w.lock.Lock()

		if err != nil {
			anyFail = true
			// TODO: log error
			continue
		}

		anySucceed = true

		status := make([]SimplifiedStatus, len(progress.Executions))
		for i, ex := range progress.Executions {
			status[i].ID = ex.ID
			status[i].Title = ex.Title
			status[i].Mode = ex.Mode
			status[i].Status = ex.Status
			status[i].RunState = ex.RunState
			status[i].CurrentOperation = ex.CurrentOperation
			if len(ex.Log) != 0 {
				anyChange = true
				for _, l := range ex.Log {
					w.dispatchEvent("execution_log", e.ID, l.Class[:1]+l.Message)
				}
			}
			ex.Log = nil
		}

		e.LastProgress = progress
		if !reflect.DeepEqual(e.LastStatus, status) {
			anyChange = true
			e.LastStatus = status
			b, _ := json.Marshal(status)
			w.dispatchEvent("execution_update", e.ID, string(b))
		}
	}

	if anyFail && !anySucceed {
		w.GetProgressDelay.Failure()
	} else if anyChange {
		w.GetProgressDelay.Success()
	} else {
		w.GetProgressDelay.NoChange()
	}
}

func (w *Watcher) getEventFilter() func(Event) bool {
	lastUpdate := make(map[int]int64)
	for _, e := range w.events {
		if e.Type == "execution_update" {
			lastUpdate[e.EID] = e.ID
		}
	}

	finished := make(map[int]bool)
	for id := range w.inactive {
		finished[id] = true
	}

	return func(e Event) bool {
		if finished[e.EID] && e.Type != "execution_destroy" {
			return false
		}

		if e.Type == "execution_update" && lastUpdate[e.EID] != e.ID {
			return false
		}

		return true
	}
}

func (w *Watcher) cleanUpEvents() {
	w.lock.Lock()
	defer w.lock.Unlock()

	if len(w.events)-w.lastClean < cleanEventsThreshold {
		return
	}

	filter := w.getEventFilter()

	toDelete := make(map[int]bool)
	for id, ended := range w.inactive {
		if ended.Add(keepEndMarkerFor).Before(time.Now()) {
			toDelete[id] = true
			delete(w.inactive, id)
		}
	}

	var events []Event
	for _, e := range w.events {
		if toDelete[e.EID] || !filter(e) {
			continue
		}

		events = append(events, e)
	}
	w.events = events
	w.lastClean = len(w.events)
}

func (w *Watcher) dispatchEvent(name string, id int, data string) {
	e := Event{
		ID:   w.nextEventID,
		Type: name,
		EID:  id,
		Data: data,
	}
	w.nextEventID++
	w.events = append(w.events, e)
	w.eventCond.Broadcast()
}
